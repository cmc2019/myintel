FROM debian:10-slim 

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    xserver-xorg-video-dummy \
    xserver-xorg-input-void \
    xserver-xorg-core \
    xinit \
    x11-xserver-utils \
    xserver-xorg-input-mouse \
    xserver-xorg-input-kbd \
    xserver-xorg-input-libinput \
    xserver-xorg-video-intel \
    spectrwm \
    scrot \
    && apt clean
